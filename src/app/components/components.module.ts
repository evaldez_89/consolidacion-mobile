import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreyenteComponent } from './creyente/creyente.component';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { CounterComponent } from './counter/counter.component';


@NgModule({
  declarations: [
    CreyenteComponent,
    HeaderComponent,
    LoginComponent,
    ProfileComponent,
    CounterComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  exports: [
    CreyenteComponent,
    HeaderComponent,
    LoginComponent,
    ProfileComponent
  ]
})
export class ComponentsModule { }
