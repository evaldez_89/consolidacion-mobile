import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent implements OnInit {

  @Input() title: string;
  @Input() count: number;
  route: string;

  constructor() { }

  ngOnInit() {
    this.route = `/${this.title.toLowerCase()}`;
  }

}
