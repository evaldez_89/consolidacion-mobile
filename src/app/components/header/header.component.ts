import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() title: string;
  @Input() authIsValid: boolean;
  @Output() logOutUser = new EventEmitter();

  constructor() { }

  ngOnInit() {}

}
