import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConsolidacionService } from 'src/app/service/consolidacion.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  submitAttemp: boolean = false;
  errorMessage: string;
  @Output() userAuthenticated = new EventEmitter();

  constructor(private apiService: ConsolidacionService,
    private formBuilder: FormBuilder,
    private localStorage: LocalstorageService,
    private navCtrl: NavController) {

    this.loginForm = this.formBuilder.group({
      email: ['custo_mon@consolidacion.com', Validators.compose([Validators.required, Validators.email])],
      password: ['admin', Validators.required]
    });
  }

  ngOnInit() { }

  cleanField(fieldName: string){
    let val:string = this.loginForm.value[fieldName].replace(/\s/g, "");
    if (fieldName != 'password') {
      val = val.toLowerCase();
    }
    this.loginForm.controls[fieldName].setValue(val);
  }

  async signIn() {
    await this.apiService.authenticate(
      this.loginForm.value['email'],
      this.loginForm.value['password'])
    .then(response => {
      this.localStorage.saveAuthenticatedUser(response).then(r => this.userAuthenticated.emit());
    }).catch(r => {
      // TODO: 401: No autorizado (Popup: Combinación Usuario clave no válido)
      console.log('hay un error');
      this.loginForm.controls.password.setValue('');
      this.errorMessage = (r.status == 401) ? "Correo/Clave No Válidos" : "Error del Sistema";
      console.log(r.status, r.statusText);
    });
  }

}
