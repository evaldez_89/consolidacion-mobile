import { Component, OnInit, Input } from '@angular/core';
import { UserInfo } from 'src/app/interfaces/response.interface';
import { ConsolidacionService } from 'src/app/service/consolidacion.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  @Input() userInfo: UserInfo;
  userImage: string;
  fullName: string;
  counters: any[] = [];

  constructor( private apiService: ConsolidacionService ) { }

  async ngOnInit() {
    // TODO: get image from api
    this.userImage = '/assets/data/profile_dev.jpg';
    this.fullName = `${this.userInfo.first_name.trim()} ${this.userInfo.last_name.trim()}`;

    if (this.userInfo.is_leader && this.userInfo.as_leader_id) {
      this.addCounter('Consolidadores', await this.apiService.getConsolidadoresCount(this.userInfo.as_leader_id));
      this.addCounter('Creyentes', await this.apiService.getCreyenteByLeaderCount(this.userInfo.as_leader_id));
    } else {
      this.addCounter('Creyentes', await this.apiService.getCreyentesCount(this.userInfo.consolidador_id));
    }

    // TODO: Mostrar cuenta de celulas
  }

  addCounter(title: string, count: number){
    if (count > 0) {
      this.counters.push({title, count})
    }
  }

}
