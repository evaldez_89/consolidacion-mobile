export interface MenuOptions {
    icon: string;
    title: string;
    url: string;
  }