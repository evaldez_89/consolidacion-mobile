export interface UserInfo {
  username: string
  first_name: string
  last_name: string
  consolidador_id: number
  is_leader: boolean
  as_leader_id?: number
  gender: string
  email: string
  token: JWToken
  leader: UserLeader
}

export interface UserLeader {
  id: number,
  name: string
}

export interface JWToken {
  access: string;
  refresh: string
}

export interface ReponseObj {
  count: number;
  next?: any;
  previous?: any;
  results: CreyenteInfo[];
}

export interface CreyenteInfo {
  url: string;
  etapa: string;
  sexo: string;
  actividad: string;
  consolidador: MinConsolidador;
  consolidador_dos?: any;
  nombre: string;
  apodo: string;
  direccion: string;
  location: string;
  cedula: string;
  fecha_nacimiento?: any;
  foto: string;
  edad: number;
  telefono: string;
  celular: string;
  otro: string;
  email: string;
  fecha_culto: string;
  reunion: string;
  nota: string;
  bautizado: boolean;
  encuentro: boolean;
  reencuentro: boolean;
  escuela_lideres: boolean;
  prox_llamada: string;
  prox_visita: string;
  fecha_creacion: string;
  celula?: any;
}

export interface MinConsolidador {
  id: number;
  nombre: string;
  url: string;
}