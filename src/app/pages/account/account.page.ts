import { Component, OnInit } from '@angular/core';
import { ConsolidacionService } from 'src/app/service/consolidacion.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { NavController } from '@ionic/angular';
import { UserInfo } from 'src/app/interfaces/response.interface';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  title: string;
  userInfo: UserInfo;

  loginForm: FormGroup
  submitAttemp: boolean = false;

  constructor(private apiService: ConsolidacionService,
              private localStorage: LocalstorageService) {
  }

  ionViewWillEnter() {
    this.loadUser();
  }

  ngOnInit() { }

  loadUser(){
    this.localStorage.getAuthenticatedUser().then(user => {
      this.userInfo = user;
      this.toggleTitle();
    });
  }

  toggleTitle(){
    if (this.userInfo) {
      this.title = 'Información de Cuenta';
    } else {
      this.title = 'Inicio de Sesión';
    }
  }

  logOut(){
    this.localStorage.logOut();
    this.loadUser();
  }

  authIsValid(){
    // TODO: verify user and token validity
    if(this.userInfo) {
      return true;
    }
    return false;
  }

}
