import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreyentesPage } from './creyentes.page';

const routes: Routes = [
  {
    path: '',
    component: CreyentesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreyentesPageRoutingModule {}
