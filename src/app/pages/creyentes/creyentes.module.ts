import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreyentesPageRoutingModule } from './creyentes-routing.module';

import { CreyentesPage } from './creyentes.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreyentesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CreyentesPage]
})
export class CreyentesPageModule {}
