import { Component, OnInit } from '@angular/core';
import { CreyenteInfo, UserInfo } from 'src/app/interfaces/response.interface';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { ConsolidacionService } from 'src/app/service/consolidacion.service';

@Component({
  selector: 'app-creyentes',
  templateUrl: './creyentes.page.html',
  styleUrls: ['./creyentes.page.scss'],
})
export class CreyentesPage implements OnInit {

  title:string = 'Lista de Creyentes';
  creyenteList: CreyenteInfo[] = [];
  userInfo: UserInfo;
  today = new Date();

  constructor(private localStorage: LocalstorageService,
              private apiService: ConsolidacionService) { }

  async ngOnInit() {
    this.userInfo = await this.localStorage.getAuthenticatedUser()

    if (this.userInfo.is_leader && this.userInfo.as_leader_id) {
      this.creyenteList = await this.apiService.getCreyenteByLeader(this.userInfo.as_leader_id);
    } else {
      this.creyenteList = await this.apiService.getCreyentes(this.userInfo.consolidador_id);
    }
  }

  dateHasPassed(date: string) {
    const appoiment = new Date(date);
    return this.today.getTime() > appoiment.getTime();
  }

}
