import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ReponseObj, UserInfo, JWToken } from '../interfaces/response.interface';
import { LocalstorageService } from './localstorage.service';


const apiUrl = environment.apiUrl;
const mapboxApiKey = environment.mapboxApiKey;

@Injectable({
  providedIn: 'root'
})
export class ConsolidacionService {

  constructor(private http: HttpClient,
              private localStorage: LocalstorageService) { }

  auth<T>(endpoint: string, body:string){
    const headers = new HttpHeaders({'Content-Type': 'application/json'})
    return this.http.post<T>(`${apiUrl}/${endpoint}`, body, { headers });
  }

  async authenticate(username: string, password: string) {
    const body = JSON.stringify({ 'username': username, 'password': password, })

    return await this.auth<UserInfo>('token/', body).toPromise();
  }

  async verifyToken(token:JWToken){
    const body = JSON.stringify({ 'token': token.access})
    const response = this.auth(`token/veryfy/`, body);
    console.log(response);
  }

  async refressAccessToken(){

  }

  async executeQuery<T>(query:string ){
    const access_token = (await this.localStorage.getToken()).access;

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${access_token}`
    });
    return this.http.get<T>(`${apiUrl}/${query}`, { headers }).toPromise();
  }

  async getCreyentes(consolidador_id: number) {
    return await this.executeQuery<ReponseObj>(`v1/creyentes?consolidador_id=${consolidador_id}`).then(resp => resp.results);
  }

  async getCreyenteByLeader(leader_id: number) {
    return await this.executeQuery<ReponseObj>(`v1/creyentes?consolidador__lider_id=${leader_id}`)
                     .then(resp => resp.results);
  }

  async getModelCount(model_name: string, filter: any){
    let query = `v1/count/${model_name}`;

    if (filter) {
      query += `/${filter.parent_name}/${filter.parent_id}`
    }

    return (await this.executeQuery<any>(query))['count'];
  }

  async getCreyentesCount(consolidador_id:number){
    return await this.getModelCount('creyente', {parent_name: 'consolidador', parent_id: consolidador_id});
  }

  async getConsolidadoresCount(leader_id:number){
    return await this.getModelCount('consolidador', {parent_name: 'lider', parent_id: leader_id});
  }

  async getCreyenteByLeaderCount(leader_id:number){
    return await this.getModelCount('creyentes', {parent_name: 'lider', parent_id: leader_id});
  }
}
