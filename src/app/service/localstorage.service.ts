import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MenuOptions } from '../interfaces/menu.interface';
import { Storage } from '@ionic/storage';
import { UserInfo, JWToken } from '../interfaces/response.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  menuOpts = new Observable<MenuOptions[]>();

  constructor(private http: HttpClient,
              private storage: Storage) { }

  getMenuOptions() {
    this.menuOpts = this.http.get<MenuOptions[]>('/assets/data/menu.json');
    return this.menuOpts;
  }

   async getAuthenticatedUser() {
    return await this.storage.get('userInfo').then(usr => usr);
  }

  async getToken(){
    let token:JWToken;
    await this.getAuthenticatedUser().then(usr => {
      token = usr.token;
    });

    return token;
  }

  async saveAuthenticatedUser(user: UserInfo){
    await this.storage.set('userInfo', user);
  }

  logOut(){
    this.storage.remove('userInfo');
  }
}
